//
//  ViewController.swift
//  DiscountCalculator
//
//  Created by Caroline Chan on 11/10/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var discountTextField: UITextField!
    @IBOutlet weak var amountSaveLabel: UILabel!
    @IBOutlet weak var finalPriceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func calculate(_ sender: Any) {
        let discountAmount: Double = calculateDiscountAmount()
        amountSaveLabel.text = "You save: \(discountAmount)"
        
        let finalPrice: Double = calculateFinalPrice()
        finalPriceLabel.text = "Final price: \(finalPrice)"
    }
    
    @IBAction func reset(_ sender: Any) {
        priceTextField.text = ""
        discountTextField.text = ""
    }
    
    func calculateDiscountAmount() -> Double{
        let price: Int = Int(priceTextField.text!) ?? 0
        let discount: Int = Int(discountTextField.text!) ?? 0
        return Double(price) * Double(discount) / 100.0
    }
    
    func calculateFinalPrice() -> Double{
        let price: Int = Int(priceTextField.text!) ?? 0
        return Double(price) - calculateDiscountAmount()
    }
}
